<?php
include_once 'ConexaoPDO.php';
include_once '../model/Projeto.php';
include_once '../lib/Logger.php';

class ProjetoDao {

	private $connection;

	function __construct() {
		
		$this->connection = getPDOConnection ();
	}

	public function find($id) {

		$stmt = $this->connection->prepare ( "
            SELECT *
             FROM projeto
             WHERE ativo = 't' and idProjeto = :id
        " );
		$stmt->bindParam ( ':id', $id );
		$stmt->execute ();
		
		// Set the fetchmode to populate an instance of 'User'
		// This enables us to use the following:
		// $user = $repository->find(1234);
		// echo $user->firstname;
		$stmt->setFetchMode ( PDO::FETCH_CLASS, 'Projeto' );
		return $stmt->fetch ();
	}

	public function findAll() {

		$stmt = $this->connection->prepare ( "SELECT * FROM Projeto where ativo = 't'" );
		$stmt->execute ();
		$stmt->setFetchMode ( PDO::FETCH_CLASS, 'Projeto' );
		
		// fetchAll() will do the same as above, but we'll have an array. ie:
		// $users = $repository->findAll();
		// echo $users[0]->firstname;
		return $stmt->fetchAll ();
	}

	public function save($projeto) {
		// If the ID is set, we're updating an existing record
		
		if (isset ( $projeto->idProjeto )) {
			return $this->update ( $projeto );
		}
		
		Logger("Salvando projeto : " . json_encode($projeto) );
		
		/* $sqlInsere = 'INSERT INTO projeto ( ';
		$values	=' ) VALUES ( ';
		 
		if(isset ( $projeto->nome )){
			$sqlInsere = $sqlInsere . ' nome,';
			$values    = $values . ':nome,';
		} */
		
		/*'
            INSERT INTO projeto
                (nome, gerente, orcamentoTotal, descricao, status, risco, dataInicio, datePrevTermino, dataTermino)
            VALUES
                (:nome, :gerente, :orcamentoTotal, :descricao, :status, :risco, :dataInicio, :datePrevTermino, :dataTermino)
        ';*/
		//Logger("QRY : " . rtrim($sqlInsere,',') . rtrim($values,',') . ' )');
		
		//$qry = rtrim($sqlInsere,','). rtrim($values,',') . ' )';
		
		$stmt = $this->connection->prepare ( '
            INSERT INTO projeto
                (nome, gerente, orcamentoTotal, descricao, status, risco, dataInicio, dataPrevTermino, colaboradores)
            VALUES
                (:nome, :gerente, :orcamentoTotal, :descricao, :status, :risco, :dataInicio, :dataPrevTermino, :colaboradores)
        ' );
		
		$stmt->bindParam ( ':nome', $projeto->nome );
		$stmt->bindParam ( ':gerente', $projeto->gerente );
		$stmt->bindParam ( ':orcamentoTotal', $projeto->orcamentoTotal );
		$stmt->bindParam ( ':descricao', $projeto->descricao );
		$stmt->bindParam ( ':status', $projeto->status );
		$stmt->bindParam ( ':risco', $projeto->risco );
		$stmt->bindParam ( ':dataInicio', date( 'Y-m-d', $projeto->dataInicio ));
		$stmt->bindParam ( ':dataPrevTermino', date( 'Y-m-d', $projeto->dataPrevTermino ));//date( 'Y-m-d H:i:s', $phpdate );		
		$stmt->bindParam ( ':colaboradores', $projeto->colaboradores );
		return $stmt->execute ();
	}

	public function update($projeto) {

		Logger("Alterando projeto : " . json_encode($projeto) . ", idProjeto : " . $projeto->idProjeto );
		
		if (! isset ( $projeto->idProjeto )) {
			// We can't update a record unless it exists...
			throw new \LogicException ( 'Cannot update user that does not yet exist in the database.' );
		}
		$stmt = $this->connection->prepare ( '
            UPDATE projeto
			   SET	nome = :nome,
				    gerente= :gerente,
					orcamentoTotal = :orcamentoTotal,
					descricao = :descricao,
					status = :status,
					risco = :risco,
					dataPrevTermino = :dataPrevTermino,
					dataInicio	=	:dataInicio,
					colaboradores = :colaboradores
			WHERE idProjeto = :idProjeto
        ' );
		
		$stmt->bindParam ( ':nome', $projeto->nome );
		$stmt->bindParam ( ':gerente', $projeto->gerente );
		$stmt->bindParam ( ':orcamentoTotal', $projeto->orcamentoTotal );
		$stmt->bindParam ( ':descricao', $projeto->descricao );
		$stmt->bindParam ( ':status', $projeto->status );
		$stmt->bindParam ( ':risco', $projeto->risco );
		$stmt->bindParam ( ':dataPrevTermino', date( 'Y-m-d', $projeto->dataPrevTermino ));
		$stmt->bindParam ( ':dataInicio', date( 'Y-m-d', $projeto->dataInicio ));
		$stmt->bindParam ( ':colaboradores', $projeto->colaboradores );
		$stmt->bindParam ( ':idProjeto', $projeto->idProjeto );
		
		return $stmt->execute ();
	}
	
	public function salvaJustificativa($justificativa) {
		// If the ID is set, we're updating an existing record
	
		Logger("Salvando justificativa : " . json_encode($justificativa) . ", idUsuario : " . $_SESSION["idUsuario"]  );
	
		$stmt = $this->connection->prepare ( '
            INSERT INTO auditoria
                (idProjeto, idUsuario, justificativa, status, data)
            VALUES
                (:idProjeto, :idUsuario, :justificativa, :status, now())
        ' );
	
		$stmt->bindParam ( ':idProjeto', $justificativa->idProjeto );
		$stmt->bindParam ( ':idUsuario', $_SESSION["idUsuario"] );
		$stmt->bindParam ( ':justificativa', $justificativa->justificativa );
		$stmt->bindParam ( ':status', $justificativa->status );
	
		return $stmt->execute ();
	}
	
	public function removeProjeto($idProjeto){
		
		Logger("Removendo projeto : idProjeto : " . $idProjeto);
		
		if (! isset ( $idProjeto )) {
			// We can't update a record unless it exists...
			throw new \LogicException ( 'Cannot update user that does not yet exist in the database.' );
		}
		$stmt = $this->connection->prepare ( "
            UPDATE projeto
			   SET	ativo = 'f'
			WHERE idProjeto = :idProjeto
        " );
		
		$stmt->bindParam ( ':idProjeto', $idProjeto );
		
		return $stmt->execute ();
	}

}

?>