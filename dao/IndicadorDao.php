<?php
include_once 'ConexaoPDO.php';
include_once '../model/Indicador.php';
include_once '../model/IndicadorAssociado.php';
include_once '../lib/Logger.php';

class IndicadorDao {

	private $connection;

	function __construct() {

		$this->connection = getPDOConnection ();
	}

	public function find($id) {

		$stmt = $this->connection->prepare ( '
            SELECT *
             FROM indicador
             WHERE idIndicador = :id
        ' );
		$stmt->bindParam ( ':id', $id );
		$stmt->execute ();
		
		// Set the fetchmode to populate an instance of 'User'
		// This enables us to use the following:
		// $user = $repository->find(1234);
		// echo $user->firstname;
		$stmt->setFetchMode ( PDO::FETCH_CLASS, 'Indicador' );
		return $stmt->fetch ();
	}

	public function findAll() {

		$stmt = $this->connection->prepare ( '
            SELECT * FROM indicador
        ' );
		$stmt->execute ();
		$stmt->setFetchMode ( PDO::FETCH_CLASS, 'Indicador' );
		
		// fetchAll() will do the same as above, but we'll have an array. ie:
		// $users = $repository->findAll();
		// echo $users[0]->firstname;
		return $stmt->fetchAll ();
	}

	public function save($indicador) {
		// If the ID is set, we're updating an existing record
		if (isset ( $indicador->idIndicador )) {
			return $this->update ( $indicador );
		}
		
		Logger ( "Salvando indicador : " . json_encode ( $indicador ) );
		
		$stmt = $this->connection->prepare ( '
            INSERT INTO indicador
                (nome, descricao)
            VALUES
                (:nome, :descricao)
        ' );
		
		$stmt->bindParam ( ':nome', $indicador->nome );
		$stmt->bindParam ( ':descricao', $indicador->descricao );
		
		return $stmt->execute ();
	}

	public function update($indicador) {

		Logger ( "Alterando indicador : " . json_encode ( $indicador ) . ", idIndicador : " . $indicador->idIndicador );
		
		if (! isset ( $indicador->idIndicador )) {
			// We can't update a record unless it exists...
			throw new \LogicException ( 'Cannot update user that does not yet exist in the database.' );
		}
		$stmt = $this->connection->prepare ( '
            UPDATE indicador
			   SET	nome = :nome,
					descricao = :descricao
			WHERE idIndicador = :idIndicador
        ' );
		
		$stmt->bindParam ( ':nome', $indicador->nome );
		$stmt->bindParam ( ':descricao', $indicador->descricao );
		$stmt->bindParam ( ':idIndicador', $indicador->idIndicador );
		
		return $stmt->execute ();
	}

	public function remove($indicador) {

		Logger ( "Removendo indicador : " . json_encode ( $indicador ) . ", idIndicador : " . $indicador->idIndicador );
		
		$stmt = $this->connection->prepare ( '
            DELETE FROM indicador
			WHERE idIndicador = :idIndicador
        ' );
		
		$stmt->bindParam ( ':idIndicador', $fase->idIndicador );
		
		return $stmt->execute ();
	}

	public function indicadoresParaAssoc($idProjeto) {

		$stmt = $this->connection->prepare ( '
            select * from indicador where idIndicador not in (select idIndicador from indicador_associado where idProjeto = :idProjeto );
        ' );
		
		$stmt->bindParam ( ':idProjeto', $idProjeto );
		
		$stmt->execute ();
		$stmt->setFetchMode ( PDO::FETCH_CLASS, 'Indicador' );
		
		return $stmt->fetchAll ();
	}

	public function indicadoresPorProjeto($idProjeto) {

		$stmt = $this->connection->prepare ( '
            select idIndicadorAssociado,nome,descricao,min,max from indicador as i , indicador_associado as ia where  i.idIndicador = ia.idIndicador and ia.idIndicador and idProjeto = :idProjeto;
        ' );
		
		$stmt->bindParam ( ':idProjeto', $idProjeto );
		
		$stmt->execute ();
		$stmt->setFetchMode ( PDO::FETCH_ASSOC );
		
		return $stmt->fetchAll ();
	}

	/*
	 * public function salvaIndicadoresAssoc($indicadoresAssociado, $idProjeto) {
	 * // If the ID is set, we're updating an existing record
	 * Logger ( "Associando indicadores : " . json_encode ( $indicadoresAssociado ) );
	 *
	 * foreach ( $indicadoresAssociado as $key => $value ) {
	 *
	 * $stmt = $this->connection->prepare ( '
	 * INSERT INTO indicador_associado
	 * (idProjeto, idIndicador, min, max)
	 * VALUES
	 * (:idProjeto, :idIndicador, :min, :max)
	 * ' );
	 *
	 * $stmt->bindParam ( ':idProjeto', $idProjeto );
	 * $stmt->bindParam ( ':idIndicador', $value->idIndicador );
	 * $stmt->bindParam ( ':min', $value->min );
	 * $stmt->bindParam ( ':max', $value->max );
	 *
	 * $stmt->execute ();
	 * }
	 *
	 * return null;
	 * }
	 */
	public function salvaIndicadorAssociado($indicadorAssociado, $idProjeto) {
		// If the ID is set, we're updating an existing record
		if (isset ( $indicadorAssociado->idIndicadorAssociado )) {
			return $this->updateIndicadorAssociado ( $indicadorAssociado, $idProjeto );
		}
		
		Logger ( "Salvando indicador associado : " . json_encode ( $indicadorAssociado ) . " idProjeto : " . $idProjeto );
		
		$stmt = $this->connection->prepare ( '
	            INSERT INTO indicador_associado
	                (idProjeto, idIndicador, min, max)
	            VALUES
	                (:idProjeto, :idIndicador, :min, :max)
        	' );
		
		$stmt->bindParam ( ':idProjeto', $idProjeto );
		$stmt->bindParam ( ':idIndicador', $indicadorAssociado->idIndicador );
		$stmt->bindParam ( ':min', $indicadorAssociado->min );
		$stmt->bindParam ( ':max', $indicadorAssociado->max );
		
		return $stmt->execute ();
	}

	public function updateIndicadorAssociado($indicadorAssociado) {

		Logger ( "Alterando indicador associado : " . json_encode ( $indicadorAssociado ) . ", idIndicador : " . $indicadorAssociado->idIndicadorAssociado );
		
		if (! isset ( $indicadorAssociado->idIndicadorAssociado )) {
			// We can't update a record unless it exists...
			throw new \LogicException ( 'Cannot update user that does not yet exist in the database.' );
		}
		$stmt = $this->connection->prepare ( '
            UPDATE indicador_associado
			   SET	min = :min,
					max = :max
			WHERE idIndicadorAssociado = :idIndicadorAssociado
        ' );
		
		$stmt->bindParam ( ':idIndicadorAssociado', $indicadorAssociado->idIndicadorAssociado );
		$stmt->bindParam ( ':min', $indicadorAssociado->min );
		$stmt->bindParam ( ':max', $indicadorAssociado->max );
		
		return $stmt->execute ();
	}

	public function removeIndicadorAssoc($idIndicadorAssoc) {

		Logger ( "Removendo indicador associado : " . json_encode ( $idIndicadorAssoc ) );
		
		$stmt = $this->connection->prepare ( '
            DELETE FROM indicador_associado
			WHERE idIndicadorAssociado = :idIndicadorAssociado
        ' );
		
		$stmt->bindParam ( ':idIndicadorAssociado', $idIndicadorAssoc );
		
		return $stmt->execute ();
	}

	public function removeIndicador($idIndicador) {

		Logger ( "Removendo indicador : " . json_encode ( $idIndicador ) );
		
		$stmt = $this->connection->prepare ( '
            DELETE FROM indicador
			WHERE idIndicador = :idIndicador
        ' );
		
		$stmt->bindParam ( ':idIndicador', $idIndicador );
		
		return $stmt->execute ();
	}

	public function indicadoresAndamento($idProjeto) {

		$stmt = $this->connection->prepare ( '
             select ia.idIndicadorAndamento,f.idFase,assoc.idIndicadorAssociado,f.nome as nomefase,i.nome,min,max,valor 
				from indicador as i , indicador_andamento as ia, indicador_associado as assoc, fase as f
				where i.idIndicador = assoc.idIndicador 
				and assoc.idIndicadorAssociado = ia.idIndicadorAssociado 
				and f.idFase = ia.idFase
				and assoc.idProjeto = :idProjeto order by f.nome ;
        ' );
		
		$stmt->bindParam ( ':idProjeto', $idProjeto );
		
		$stmt->execute ();
		$stmt->setFetchMode ( PDO::FETCH_ASSOC );
		
		return $stmt->fetchAll ();
	}

	public function relatorioIndicadoresAndamento() {

		$stmt = $this->connection->prepare ( '
              select p.nome nomeProjeto,i.nome nomeIndicador,f.nome nomeFase,ia.min,ia.max,ian.valor
				from projeto  p, indicador_associado ia,indicador_andamento ian, fase f, indicador i
				where p.idProjeto = ia.idProjeto
				  and ia.idIndicador = i.idIndicador
				  and ia.idIndicadorAssociado = ian.idIndicadorAssociado
				  and ian.idFase = f.idFase ;
        ' );
		
		$stmt->execute ();
		$stmt->setFetchMode ( PDO::FETCH_ASSOC );
		
		return $stmt->fetchAll ();
	}

	public function salvaIndicadorAndamento($indicadorAndamento) {
		// If the ID is set, we're updating an existing record
		if (isset ( $indicadorAndamento->idIndicadorAndamento )) {
			return $this->updateIndicadorAndamento ( $indicadorAndamento );
		}
		
		Logger ( "Salvando indicador andamento : " . json_encode ( $indicadorAndamento ) );
		
		$stmt = $this->connection->prepare ( '
	            INSERT INTO indicador_andamento
	                (idIndicadorAssociado, idFase, valor)
	            VALUES
	                (:idIndicadorAssociado, :idFase, :valor)
        	' );
		
		$stmt->bindParam ( ':idIndicadorAssociado', $indicadorAndamento->indicadorAssociado->idIndicadorAssociado );
		$stmt->bindParam ( ':idFase', $indicadorAndamento->fase->idFase );
		$stmt->bindParam ( ':valor', $indicadorAndamento->valor );
		
		return $stmt->execute ();
	}

	public function updateIndicadorAndamento($indicadorAndamento) {

		Logger ( "Alterando indicador andamento : " . json_encode ( $indicadorAndamento ) );
		
		if (! isset ( $indicadorAndamento->idIndicadorAndamento )) {
			// We can't update a record unless it exists...
			throw new \LogicException ( 'Cannot update indicador de andamento that does not yet exist in the database.' );
		}
		$stmt = $this->connection->prepare ( '
            UPDATE indicador_andamento
			   SET	valor = :valor,
					idFase = :idFase,
					idIndicadorAssociado = :idIndicadorAssociado 
			WHERE idIndicadorAndamento = :idIndicadorAndamento
        ' );
		
		$stmt->bindParam ( ':idIndicadorAndamento', $indicadorAndamento->idIndicadorAndamento );
		$stmt->bindParam ( ':idIndicadorAssociado', $indicadorAndamento->idIndicadorAssociado );
		$stmt->bindParam ( ':valor', $indicadorAndamento->valor );
		$stmt->bindParam ( ':idFase', $indicadorAndamento->idFase );
		
		return $stmt->execute ();
	}

}
?>