<?php
include_once 'ConexaoPDO.php';
include_once '../model/Fase.php';
include_once '../lib/Logger.php';

class FaseDao {

	private $connection;

	function __construct() {

		$this->connection = getPDOConnection ();
	}

	public function find($id) {

		$stmt = $this->connection->prepare ( '
            SELECT *
             FROM fase
             WHERE idFase = :id
        ' );
		$stmt->bindParam ( ':id', $id );
		$stmt->execute ();
		
		// Set the fetchmode to populate an instance of 'User'
		// This enables us to use the following:
		// $user = $repository->find(1234);
		// echo $user->firstname;
		$stmt->setFetchMode ( PDO::FETCH_CLASS, 'Fase' );
		return $stmt->fetch ();
	}

	public function findAll( $idProjeto ) {

		$stmt = $this->connection->prepare ( '
            SELECT * FROM fase where idProjeto = :idProjeto
        ' );
		
		$stmt->bindParam ( ':idProjeto', $idProjeto );
		
		$stmt->execute ();
		$stmt->setFetchMode ( PDO::FETCH_CLASS, 'Fase' );
		
		// fetchAll() will do the same as above, but we'll have an array. ie:
		// $users = $repository->findAll();
		// echo $users[0]->firstname;
		return $stmt->fetchAll ();
	}

	public function save($fase, $idProjeto) {
		// If the ID is set, we're updating an existing record
		if (isset ( $fase->idFase )) {
			return $this->update ( $fase );
		}
		
		Logger ( "Salvando fase : " . json_encode ( $fase ) );
		
		$stmt = $this->connection->prepare ( '
            INSERT INTO fase
                (idProjeto, nome, obs)
            VALUES
                (:idProjeto, :nome, :obs)
        ' );
		
		$stmt->bindParam ( ':idProjeto', $idProjeto );
		$stmt->bindParam ( ':nome', $fase->nome );
		$stmt->bindParam ( ':obs', $fase->obs );
		
		return $stmt->execute ();
	}

	public function update($fase) {

		Logger ( "Alterando fase : " . json_encode ( $fase ) . ", idFase : " . $fase->idFase );
		
		if (! isset ( $fase->idFase )) {
			// We can't update a record unless it exists...
			throw new \LogicException ( 'Cannot update user that does not yet exist in the database.' );
		}
		$stmt = $this->connection->prepare ( '
            UPDATE fase
			   SET	nome = :nome,
					obs = :obs
			WHERE idFase = :idFase
        ' );
		
		$stmt->bindParam ( ':nome', $fase->nome );
		$stmt->bindParam ( ':obs', $fase->obs );
		$stmt->bindParam ( ':idFase', $fase->idFase );
		
		return $stmt->execute ();
	}
	
	public function remove($idFase) {
	
		Logger ( "Removendo fase : " . json_encode ( $idFase ) );
	
		$stmt = $this->connection->prepare ( '
            DELETE FROM fase
			WHERE idFase = :idFase
        ' );
	
		$stmt->bindParam ( ':idFase', $idFase );
	
		return $stmt->execute ();
	}

	public function fasesPorProjeto($idProjeto) {

		$stmt = $this->connection->prepare ( '
            select * from fase where idProjeto = :idProjeto;
        ' );
		
		$stmt->bindParam ( ':idProjeto', $idProjeto );
		
		$stmt->execute ();
		$stmt->setFetchMode ( PDO::FETCH_ASSOC );
		
		return $stmt->fetchAll ();
	}
}
?>