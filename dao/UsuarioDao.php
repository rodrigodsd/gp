<?php
include_once 'ConexaoPDO.php';
include_once '../model/Usuario.php';
include_once '../lib/Logger.php';

class UsuarioDao {

	private $connection;

	function __construct() {

		$this->connection = getPDOConnection ();
	}

	function insere(Usuario $usuario) {

		Logger ( "Salvando usuario : " .  json_encode($usuario)  );
		
		$stmt = $this->connection->prepare ( '
            INSERT INTO usuario
                (nome, perfil, login, senha)
            VALUES
                (:nome, :perfil, :login, :senha)
        ' );
		Logger("NOME : " . $usuario->getNome() );
		
		$stmt->bindParam ( ':nome', $usuario->getNome() );
		$stmt->bindParam ( ':perfil', $usuario->getPerfil()  );
		$stmt->bindParam ( ':login', $usuario->getLogin()  );
		$stmt->bindParam ( ':senha', $usuario->getSenha()  );
		
		return $stmt->execute ();
	}

	function delete($idUsuario) {

		Logger ( "Removendo usuario : idUsuario : " . $idUsuario );
		
		$stmt = $this->connection->prepare ( 'DELETE FROM usuario WHERE idUsuario = :idUsuario ' );
		
		$stmt->bindParam ( ':idUsuario', $idUsuario );
		
		return $stmt->execute ();
	}

	function update($usuario) {

		Logger ( "Alterando usuario : " . json_encode ( $usuario ) . ", idUsuario : " . $usuario->idUsuario );
		
		$stmt = $this->connection->prepare ( '
            UPDATE usuario
			   SET	nome = :nome,
					perfil = :perfil
			WHERE idUsuario = :idUsuario
        ' );
		
		$stmt->bindParam ( ':nome', $usuario->nome );
		$stmt->bindParam ( ':perfil', $usuario->perfil );
		$stmt->bindParam ( ':idUsuario', $usuario->idUsuario );
		
		return $stmt->execute ();
	}

	public function find($id) {

		$stmt = $this->connection->prepare ( '
            SELECT *
             FROM usuario
             WHERE idUsuario = :id
        ' );
		$stmt->bindParam ( ':id', $id );
		$stmt->execute ();
		
		$stmt->setFetchMode ( PDO::FETCH_ASSOC );
		return $stmt->fetch ();
	}

	public function findAll() {

		$stmt = $this->connection->prepare ( '
            SELECT * FROM usuario
        ' );
		
		$stmt->execute ();
		$stmt->setFetchMode ( PDO::FETCH_ASSOC );
		
		// fetchAll() will do the same as above, but we'll have an array. ie:
		// $users = $repository->findAll();
		// echo $users[0]->firstname;
		return $stmt->fetchAll ();
	}

}

?>