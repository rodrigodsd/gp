<div class="row">

	<p>
		<a data-ng-click="update(null)" href="javascript:;"	class="btn btn-primary">Novo Indicador</a>
	</p>
	<div class="col-md-12" ng-show="indicadores.length > 0">

		<table class="table table-striped ">
			<thead>
				<th>ID&nbsp;</th>
				<th>Nome&nbsp;</th>
				<th>Descricao&nbsp;</th>
				<th>&nbsp;</th>
			</thead>
			<tbody>
				<tr ng-repeat="data in indicadores">
					<td>{{data.idIndicador}}</td>
					<td>{{data.nome}}</td>
					<td>{{data.descricao}}</td>
					<td>
						<a href ng-click="update(data)" class="btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
						<a href ng-click="removeIndicador(data.idIndicador)" class="btn"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
					</td>

				</tr>
			</tbody>
		</table>
	</div>
	<div class="col-md-12" ng-show="indicadores.length == 0">
		<div class="col-md-12">
			<h4>No projects found</h4>
		</div>
	</div>
</div>