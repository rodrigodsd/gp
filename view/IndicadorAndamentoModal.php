<form class="form-horizontal">
	<fieldset>

		<!-- Form Name -->
		<legend>Indicador {{indicador.nome}}</legend>
		<div class="modal-body">
			<div class="form-group">
				<label class="col-md-4 control-label" for="indicadorSelected">Indicadores</label>
				<div class="col-md-4">
					<select id="indicadorSelected" name="indicadorSelected" class="form-control" ng-model="indicadorAndamento.idIndicadorAssociado" required>
						<option ng-repeat="indicadorAssociado in indicadoresAssociados" value="{{indicadorAssociado.idIndicadorAssociado}}" >{{indicadorAssociado.nome}}</option>
					</select>
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-md-4 control-label" for="faseSelected">Fases</label>
				<div class="col-md-4">
					<select id="faseSelected" name="faseSelected" class="form-control" ng-model="indicadorAndamento.idFase"  required>
						 <option ng-repeat="fase in fases" value="{{fase.idFase}}">{{fase.nome}}</option>
					</select>
				</div>
			</div>

			<!-- Text input-->
			<div class="form-group">
				<label class="col-md-4 control-label" for="valor">Valor</label>
				<div class="col-md-4">
					<input id="valor" name="valor" ng-model="indicadorAndamento.valor" type="text" placeholder="valor" class="form-control input-md" required>
				</div>
			</div>

		</div>

		<div class="modal-footer">
			<button class="btn btn-primary" type="button" ng-click="ok()">OK</button>
			<button class="btn btn-warning" type="button" ng-click="cancel()">Cancel</button>
		</div>

	</fieldset>
</form>
