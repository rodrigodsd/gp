<div class="row">

	<p>
		<a data-ng-click="update(null)" href="javascript:;"	class="btn btn-primary">Novo Projeto</a>
	</p>
	<div class="col-md-12" ng-show="projetos.length > 0">

		<table class="table table-striped ">
			<thead>
				<th>ID&nbsp;</th>
				<th>Nome&nbsp;</th>
				<th>Gerente&nbsp;</th>
				<th>Descrição&nbsp;</th>
				<th>Status&nbsp;</th>
				<th>&nbsp;</th>
			</thead>
			<tbody>
				<tr ng-repeat="data in projetos">
					<td>{{data.idProjeto}}</td>
					<td>{{data.nome}}</td>
					<td>{{data.gerente}}</td>
					<td>{{data.descricao}}</td>
					<td>{{translateStatus(data.status);}}</td>
					<td>
						<a href ng-click="update(data)" class="btn" ng-if="perfil  == 'GERENTE' || perfil  == 'ADMIN'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> 
					    <a href ng-click="removeProjeto(data.idProjeto)" class="btn" ng-if="perfil  == 'GERENTE' || perfil  == 'ADMIN'"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
					    <a href="#/projetoDetalhe/{{data.idProjeto}}" class="btn" ng-if="perfil  == 'GERENTE' || perfil  == 'ADMIN'"><i class="fa fa-cog" aria-hidden="true"></i></a>
					    <a href="#/andamento/{{data.idProjeto}}" class="btn"><i class="fa fa-line-chart" aria-hidden="true"></i></i></a>				
					</td>

				</tr>
			</tbody>
		</table>
	</div>
	<div class="col-md-12" ng-show="projetos.length == 0">
		<div class="col-md-12">
			<h4>No projects found</h4>
		</div>
	</div>
</div>