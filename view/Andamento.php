<div class="row">

	<p>
		<a data-ng-click="update(null)" href="javascript:;" class="btn btn-primary">Adicionar Indicador de Andamento</a>
	</p>
	<div class="col-md-12" ng-show="indicadoresAndamento.length > 0">

		<table class="table table-striped ">
			<thead>
				<th>Nome&nbsp;</th>
				<th class="text-center">Fase</th>
				<th class="text-center">Mínimo&nbsp;</th>
				<th class="text-center">Máximo</th>
				<th class="text-center">Posição&nbsp;</th>				
				<th>Progresso</th>	
				<th>&nbsp;</th>
			</thead>
			<tbody>
				<tr ng-repeat="data in indicadoresAndamento">
					<td>{{data.nome}}</td>
					<td class="text-center">{{data.nomefase}}</td>
					<td class="text-center">{{data.min}}</td>
					<td class="text-center">{{data.max}}</td>
					<td class="text-center">{{data.valor}}</td>
					<td>
						<div>
							<span>{{data.min}}</span>
							<span class="pull-right">{{data.max}}</span>
						</div>
						<div class="progress">
							<uib-progressbar class="progress-striped active" value="data.valor" max="data.max" min="data.min" type="{{data.type}}"><i ng-show="{{data.atencao}}">!!! Atenção !!!</i></uib-progressbar>
						</div>
						
					</td>
					
					<td>
						<a href ng-click="update(data)" class="btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
						<a href ng-click="removeIndicadorAndamento(indicadorAndamento.idIndicadorAndamento)" class="btn" ng-if="perfil  == 'GERENTE' || perfil  == 'ADMIN'"><i class="fa fa-trash-o" aria-hidden="true"></i></a>	
					</td>

				</tr>
			</tbody>
		</table>
	</div>
	<div class="col-md-12" ng-show="indicadoresAndamento.length == 0">
		<div class="col-md-12">
			<h4>No projects found</h4>
		</div>
	</div>
</div>