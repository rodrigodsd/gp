<div class="row">

	<div class="col-md-12" ng-show="usuarios.length > 0">

		<table class="table table-striped ">
			<thead>
				<th>ID&nbsp;</th>
				<th>Nome&nbsp;</th>
				<th>Login&nbsp;</th>
				<th>Perfil&nbsp;</th>
			</thead>
			<tbody>
				<tr ng-repeat="data in usuarios">
					<td>{{data.idUsuario}}</td>
					<td>{{data.nome}}</td>
					<td>{{data.login}}</td>
					<td>{{data.perfil}}</td>
					<td>
						<a href ng-click="update(data)" class="btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> 
						<a href ng-click="removeUsuario(data.idUsuario)" class="btn"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="col-md-12" ng-show="usuarios.length == 0">
		<div class="col-md-12">
			<h4>No projects found</h4>
		</div>
	</div>
</div>