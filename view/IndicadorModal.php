<form class="form-horizontal"  name="indicadorModalForm">
<fieldset>

<!-- Form Name -->
<legend>Projeto</legend>

<!-- Text input-->
<div class="form-group" ng-class="{ 'has-error': indicadorModalForm.nome.$invalid }">
  <label class="col-md-4 control-label" for="nome">Nome</label>  
  <div class="col-md-4">
  <input id="nome" name="nome" ng-model="indicador.nome" type="text" placeholder="Nome" class="form-control input-md" required>
  </div>
</div>

<!-- Text input-->
<div class="form-group" ng-class="{ 'has-error': indicadorModalForm.descricao.$invalid }">
  <label class="col-md-4 control-label" for="descricao">Descricao</label>  
  <div class="col-md-4">
  	<textarea class="form-control" id="descricao" name="descricao" ng-model="indicador.descricao" required></textarea>
  </div>
</div>


<div class="modal-footer">
	<button class="btn btn-primary" type="button" ng-click="ok()" ng-disabled="indicadorModalForm.$invalid">OK</button>
	<button class="btn btn-warning" type="button" ng-click="cancel()">Cancel</button>
</div>

</fieldset>
</form>
