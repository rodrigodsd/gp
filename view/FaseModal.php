<form class="form-horizontal"  name="faseModalForm">
<fieldset>

<!-- Form Name -->
<legend>Fase</legend>

<!-- Text input-->
<div class="form-group" ng-class="{ 'has-error': faseModalForm.nome.$invalid }">
  <label class="col-md-4 control-label" for="nome">Nome</label>  
  <div class="col-md-4">
  <input id="nome" name="nome" ng-model="fase.nome" type="text" placeholder="Nome" class="form-control input-md" required>
  </div>
</div>

<!-- Text input-->
<div class="form-group" ng-class="{ 'has-error': faseModalForm.obs.$invalid }">
  <label class="col-md-4 control-label" for="obs">Descricao</label>  
  <div class="col-md-4">
  	<textarea class="form-control" id="obs" name="obs" ng-model="fase.obs" required></textarea>
  </div>
</div>


<div class="modal-footer">
	<button class="btn btn-primary" type="button" ng-click="ok()" ng-disabled="faseModalForm.$invalid">OK</button>
	<button class="btn btn-warning" type="button" ng-click="cancel()">Cancel</button>
</div>

</fieldset>
</form>
