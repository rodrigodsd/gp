<?php 
	include '../controller/Sessao.php';
?>

<!DOCTYPE html>
<html lang="en" ng-app="gpApp" data-ng-init="perfil='<?=$_SESSION["perfil"]?>'">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="Rodrigo Duarte">

<title>Gerenciador de Portifólios - Principal</title>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
<link rel="stylesheet" href="../lib/font-awesome-4.6.3/css/font-awesome.min.css" rel="stylesheet">
<link rel="stylesheet" href="../lib/ui-grid-3.2.6/ui-grid.min.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="../css/principal.css" rel="stylesheet">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script	src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular.min.js"></script>
<script	src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular-animate.js"></script>
 <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular-touch.js"></script>
<script	src="https://cdnjs.cloudflare.com/ajax/libs/angular-i18n/1.2.15/angular-locale_pt-br.js"></script>
<script src="https://code.angularjs.org/1.4.3/angular-route.min.js"></script>
<script src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-2.0.0.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<script src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-2.0.0.js"></script>

<script src="../lib/ui-grid-3.2.6/ui-grid.min.js"></script>

<!-- Custom JS -->
<script src="../js/projeto.js"></script>
<script src="../js/indicador.js"></script>
<script src="../js/andamento.js"></script>
<script src="../js/routes.js"></script>
<script src="../js/services.js"></script>
<script src="../js/usuario.js"></script>
<script src="../js/relatorio.js"></script>

</head>

<body>

	<!-- Navigation -->
	<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#/projetoLista">Gerenciador de Portifólios</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse"  id="bs-example-navbar-collapse-1" ng-controller="HeaderCtrl">
      <ul class="nav navbar-nav">
        <li ng-class="{ active: isActive('/projetoLista')}"><a href="#/projetoLista">Projetos <span class="sr-only">Projetos</span></a></li>
        <li ng-class="{ active: isActive('/indicadorLista')}" ng-if="perfil  == 'GERENTE' || perfil  == 'ADMIN'"><a href="#/indicadorLista">Indicadores<span class="sr-only">Indicadores</span></a></li>
        <li ng-class="{ active: isActive('/usuarioLista')}"   ng-if="perfil  == 'ADMIN'"><a href="#/usuarioLista">Usuários<span class="sr-only">Usuários</span></a></li>
        <li ng-class="{ active: isActive('/relatorioLista')}"  ng-if="perfil  == 'DIRECAO' || perfil  == 'ADMIN'"><a href="#/relatorioLista">Relatórios<span class="sr-only">Relatórios</span></a></li>
      </ul>
    
      <ul class="nav navbar-nav navbar-right">
       <li><a href="#"><?=$_SESSION["login"]?></a></li>
       <li><a href="/gp/controller/Logout.php">Logout</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

	<!-- Page Content -->
	<div class="container">
		<div ng-view></div>
	<div class="container">

		<hr>

		<!-- Footer -->
		<footer>
			<div class="row">
				<div class="col-lg-12">
					<p>Copyright &copy; Your Website 2016</p>
				</div>
			</div>
		</footer>

	</div>
	<!-- /.container -->

</body>

</html>
