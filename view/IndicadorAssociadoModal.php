<form class="form-horizontal">
	<fieldset>

		<!-- Form Name -->
		<legend>Indicadores</legend>
		<div class="modal-body">
			<div class="form-group" ng-if="!indicadorAssociado.idIndicadorAssociado">
				<label class="col-md-4 control-label" for="indicadorSelected">Indicadores</label>
				<div class="col-md-4">
					<select id="indicadorSelected" name="indicadorSelected" class="form-control" ng-model="indicadorSelected" ng-change="print(indicadorSelected)"
						ng-options="indicador.nome for indicador in indicadores" required>
						<option value="">Selecione um indicador.</option>
					</select>
				</div>
			</div>

			<div class="form-group">
				<h4 class="col-md-8" ng-if="indicadorAssociado.idIndicadorAssociado">Indicador : {{indicadorAssociado.nome}}</h4>
			</div>

			<!-- Text input-->
			<div class="form-group">
				<label class="col-md-4 control-label" for="min">Min</label>
				<div class="col-md-4">
					<input id="min" name="min" ng-model="indicadorAssociado.min" type="text" placeholder="min" class="form-control input-md" required>
				</div>
			</div>

			<!-- Text input-->
			<!-- Text input-->
			<div class="form-group">
				<label class="col-md-4 control-label" for="max">Max</label>
				<div class="col-md-4">
					<input id="max" name="max" ng-model="indicadorAssociado.max" type="text" placeholder="max" class="form-control input-md" required>
				</div>
			</div>

		</div>

		<div class="modal-footer">
			<button class="btn btn-primary" type="button" ng-click="ok()">OK</button>
			<button class="btn btn-warning" type="button" ng-click="cancel()">Cancel</button>
		</div>

	</fieldset>
</form>
