<div class="row">
	<h4>Settings</h4>
	<div class="form-group checkbox">
		<label> <input type="checkbox" ng-model="acompanhamento"> Acompanhamento Semanal
		</label><i class="fa fa-info-circle" uib-tooltip="Envia um e-mail no dia selecionado." aria-hidden="true"></i>
	</div>
	<!-- Select Basic -->
	<div class="form-group" ng-show="acompanhamento">
		<label class="col-md-2 control-label" for="status">Dia Da Semana</label>
		<div class="col-md-2">
			<select id="status" name="status" class="form-control" ng-model="diaEmailSemanal" required>
				<option value="1" selected="selected">Segunda</option>
				<option value="2">Terça</option>
				<option value="3">Quarta</option>
				<option value="4">Quinta</option>
				<option value="5">Sexta</option>
			</select>
		</div>
		<button class="btn btn-primary" type="button" ng-click="salvaCompanhamento()" ng-disabled="diaEmailSemanal.$invalid">OK</button>
	</div>
</div>


<div class="row">

	<h4>Indicadores</h4>
	<p>
		<a data-ng-click="updateIndicador()" href="javascript:;" class="btn btn-primary">Associar Indicador</a>
	</p>
	<div class="col-md-12" ng-show="indicadores.length > 0">
		<table class="table table-striped ">
			<thead>
				<th>Nome&nbsp;</th>
				<th>Descrição&nbsp;</th>
				<th>Min&nbsp;</th>
				<th>Max&nbsp;</th>
				<th>&nbsp;</th>
			</thead>
			<tbody>
				<tr ng-repeat="indicador in indicadores">
					<td>{{indicador.nome}}</td>
					<td>{{indicador.descricao}}</td>
					<td>{{indicador.min}}</td>
					<td>{{indicador.max}}</td>
					<td><a href ng-click="updateIndicador(indicador)" class="btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
					    <a href ng-click="removeIndicador(indicador.idIndicadorAssociado)" class="btn"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>

				</tr>
			</tbody>
		</table>
	</div>
	<div class="col-md-12" ng-show="indicadores.length == 0">
		<div class="col-md-12">
			<h4>No projects found</h4>
		</div>
	</div>
</div>

<div class="row">

	<h4>Fases</h4>
	<p>
		<a data-ng-click="updateFase()" href="javascript:;" class="btn btn-primary">Adicionar Fase</a>
	</p>
	<div class="col-md-12" ng-show="fases.length > 0">
		<table class="table table-striped ">
			<thead>
				<th>Nome&nbsp;</th>
				<th>Observação&nbsp;</th>
				<th>&nbsp;</th>
			</thead>
			<tbody>
				<tr ng-repeat="fase in fases">
					<td>{{fase.nome}}</td>
					<td>{{fase.obs}}</td>
					<td><a href ng-click="updateFase(fase)" class="btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> <a href ng-click="removeFase(fase.idFase)" class="btn"><i
							class="fa fa-trash-o" aria-hidden="true"></i></a></td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="col-md-12" ng-show="fases.length == 0">
		<div class="col-md-12">
			<h4>No projects found</h4>
		</div>
	</div>
</div>