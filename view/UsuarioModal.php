<form class="form-horizontal" name="usuarioModalForm">
	<fieldset>

		<!-- Form Name -->
		<legend>Usuário</legend>

		<!-- Text input-->
		<div class="form-group" ng-class="{ 'has-error': usuarioModalForm.nome.$invalid }">
			<label class="col-md-4 control-label" for="nome">Nome</label>
			<div class="col-md-4">
				<h2>{{usuario.nome}}</h2>
			</div>
		</div>

		<!-- Text input-->
		<div class="form-group" ng-class="{ 'has-error': usuarioModalForm.perfil.$invalid }">
			<label class="col-md-4 control-label" for="perfil">Perfil</label>
			<div class="col-md-4">
				<input id="perfil" name="perfil" ng-model="usuario.perfil" type="text" placeholder="Nome" class="form-control input-md" required>
			</div>
		</div>

		<div class="modal-footer">
			<button class="btn btn-primary" type="button" ng-click="ok()" ng-disabled="usuarioModalForm.$invalid">OK</button>
			<button class="btn btn-warning" type="button" ng-click="cancel()">Cancel</button>
		</div>

	</fieldset>
</form>
