<div class="row">
  <div class="col-lg-6">
    <div class="input-group input-group-sm pull-left">
      <input type="text" class="form-control" placeholder="Search for..." ng-model="projetoFilter">
    </div><!-- /input-group -->
  </div><!-- /.col-lg-6 -->
<div class="row">

	<table class="table table-striped ">
		<thead>
			<th>Projeto&nbsp;</th>
			<th>Indicador</th>
			<th>Fase&nbsp;</th>
			<th>Progresso</th>
		</thead>
		<tbody>
			<tr ng-repeat="data in indicadoresAndamento | filter : projetoFilter">
				<td>{{data.nomeProjeto}}</td>
				<td>{{data.nomeIndicador}}</td>
				<td>{{data.nomeFase}}</td>
				<td>
					<div>
						<span>{{data.min}}</span> <span class="pull-right">{{data.max}}</span>
					</div>
					<div class="progress">
						<uib-progressbar value="data.valor" max="data.max" min="data.min" type="{{data.type}}">{{data.valor}}</uib-progressbar>
					</div>

				</td>
			</tr>
		</tbody>
	</table>
</div>