<form class="form-horizontal"  name="projetoModalForm">
<fieldset>

<!-- Form Name -->
<legend>Projeto</legend>

<!-- Text input-->
<div class="form-group" ng-class="{ 'has-error': projetoModalForm.nome.$invalid }">
  <label class="col-md-4 control-label" for="nome">Nome</label>  
  <div class="col-md-4">
  <input id="nome" name="nome" ng-model="projeto.nome" type="text" placeholder="Nome" class="form-control input-md" required>
  </div>
</div>

<!-- Text input-->
<div class="form-group" ng-class="{ 'has-error': projetoModalForm.gerente.$invalid }">
  <label class="col-md-4 control-label" for="gerente">Gerente</label>  
  <div class="col-md-4">
  <input id="gerente" name="gerente" type="text" placeholder="Gerente" class="form-control input-md" ng-model="projeto.gerente" required>  
  </div>
</div>

<!-- Textarea -->
<div class="form-group" ng-class="{ 'has-error': projetoModalForm.descricao.$invalid }">
  <label class="col-md-4 control-label" for="descricao">Descrição</label>
  <div class="col-md-4">                     
    <textarea class="form-control" id="descricao" name="descricao" ng-model="projeto.descricao" required></textarea>
  </div>
</div>

<!-- Select Basic -->
<div class="form-group" ng-class="{ 'has-error': projetoModalForm.status.$invalid }">
  <label class="col-md-4 control-label" for="status">Status</label>
  <div class="col-md-4">
   <select id="status" name="status" class="form-control" ng-model="projeto.status"  required>
      <option value="1" selected="selected">em análise</option>
      <option value="2">análise realizada</option>
      <option value="3">análise aprovada</option>
      <option value="4">iniciado</option>
      <option value="5">planejado</option>
      <option value="6">em andamento</option>
      <option value="7">encerrado</option>
      <option value="8">cancelado</option>   
    </select>
  </div>
</div>



<!-- Text input-->
<div class="form-group" ng-show="projeto.status == 3 || projeto.status == 8" ng-class="{ 'has-error': projetoModalForm.orcamentoTotal.$invalid }">
  <label class="col-md-4 control-label" for="justificativa">Justificativa</label>  
  <div class="col-md-4">
  <textarea class="form-control" id="justificativa" name="justificativa" ng-model="projeto.justificativa" ng-required="projeto.status == 3 || projeto.status == 8"></textarea>
  </div>
</div>

<!-- Select Basic -->
<div class="form-group" ng-class="{ 'has-error': projetoModalForm.risco.$invalid }">
  <label class="col-md-4 control-label" for="risco">Risco</label>
  <div class="col-md-4">
    <select id="risco" name="risco" class="form-control" ng-model="projeto.risco"  required>
       <option value="1" selected="selected">Baixo</option>
      <option value="2">Médio</option>
      <option value="3">Alto</option>
    </select>
  </div>
</div>

<!-- Textarea -->
<div class="form-group" ng-class="{ 'has-error': projetoModalForm.dataPrevTermino.$invalid }">
  <label class="col-md-4 control-label" for="dataPrevTermino">Data Previsão Término</label>
  <div class="col-md-4">
  	<input id="dataPrevTermino" name="dataPrevTermino" type="date" placeholder="dd-mm-aaaa" class="form-control input-md" ng-model="projeto.dataPrevTermino" required>                     
  </div>
</div>

<!-- Textarea -->
<div class="form-group" ng-class="{ 'has-error': projetoModalForm.dataInicio.$invalid }">
  <label class="col-md-4 control-label" for="dataInicio">Data Início</label>
  <div class="col-md-4">
  	<input id="dataInicio" name="dataInicio" type="date" placeholder="dd-mm-aaaa" class="form-control input-md" ng-model="projeto.dataInicio" required>                     
  </div>
</div>

<!-- Textarea -->
<div class="form-group" ng-class="{ 'has-error': projetoModalForm.colaboradores.$invalid }">
  <label class="col-md-4 control-label" for="colaboradores">Colaboradores</label>
  <div class="col-md-4">                     
    <textarea class="form-control" id="colaboradores" name="colaboradores" ng-model="projeto.colaboradores" required></textarea>
  </div>
</div>

<div class="modal-footer">
	<button class="btn btn-primary" type="button" ng-click="ok()" ng-disabled="projetoModalForm.$invalid">OK</button>
	<button class="btn btn-warning" type="button" ng-click="cancel()">Cancel</button>
</div>

</fieldset>
</form>
