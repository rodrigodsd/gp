// CONTROLLERS
app.controller('HeaderCtrl', function($scope, $location) {

	$scope.isActive = function(viewLocation) {

		return viewLocation === $location.path();
	};
});

app.controller('relatorioCtrl', function($scope, $location, indicadorServicos) {
	// LOAD DATA

	$scope.carregaIndicadoresAndamento = function() {
		indicadorServicos.buscaRelatorioIndicadoresAndamento().then(
				function(data) {

					for (var int = 0; int < data.data.length; int++) {
						var array_element = data.data[int];
						if (parseInt(array_element.valor) > parseInt(array_element.max)	|| parseInt(array_element.valor) < parseInt(array_element.min)) {
							array_element.type = 'danger';
							array_element.atencao = true;
						} else {
							array_element.type = 'info';
							array_element.atencao = false;
						}
					}

					$scope.indicadoresAndamento = data.data;
				});
	}

	$scope.carregaIndicadoresAndamento();
});
