// ROUTES

app.config(function($routeProvider) {

	$routeProvider.when('/projetoLista', {
		templateUrl : 'ProjetoLista.php',
		controller : 'projetoListaCtrl'
	}).when('/projetoDetalhe/:idProjeto', {
		templateUrl : 'ProjetoDetalhe.php',
		controller : 'projetoDetalheCtrl'
	}).when('/indicadorLista', {
		templateUrl : 'IndicadorLista.php',
		controller : 'indicadorListaCtrl'
	}).when('/andamento/:idProjeto', {
		templateUrl : 'Andamento.php',
		controller : 'andamentoCtrl'
	}).when('/usuarioLista', {
		templateUrl : 'Usuario.php',
		controller : 'usuarioCtrl'
	}).when('/relatorioLista', {
		templateUrl : 'Relatorio.php',
		controller : 'relatorioCtrl'
	}).otherwise({
		redirectTo : '/'
	});
});