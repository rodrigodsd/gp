app.controller('andamentoCtrl', function($scope, $http, $log, indicadorServicos, $uibModal, $routeParams) {

	// LOAD DATA

	$scope.carregaIndicadoresAndamento = function() {
		indicadorServicos.buscaIndicadoresAndamento($routeParams.idProjeto).then(
				function(data) {

					for (var int = 0; int < data.data.length; int++) {
						var array_element = data.data[int];
						if (parseInt(array_element.valor) > parseInt(array_element.max)
								|| parseInt(array_element.valor) < parseInt(array_element.min)) {
							array_element.type = 'danger';
							array_element.atencao = true;
						} else {
							array_element.type = 'info';
							array_element.atencao = false;
						}
					}

					$scope.indicadoresAndamento = data.data;
				});
	}

	$scope.carregaIndicadoresAndamento();

	// MODAL DA GRID DE INDICADORES

	$scope.update = function(indicadorAndamento) {
		var modalInstance = $uibModal.open({
			animation : $scope.animationsEnabled,
			templateUrl : 'IndicadorAndamentoModal.php',
			controller : 'IndicadorAndamentoModalCtrl',
			resolve : {
				idProjeto : function() {
					return $routeParams.idProjeto;
				},
				indicadorAndamento : function() {
					return indicadorAndamento;
				}
			}
		});

		modalInstance.result.then(function(indicadorAndamento) {
			indicadorServicos.salvaIndicadorAndamento(indicadorAndamento).then(function() {
				$scope.carregaIndicadoresAndamento();
			});
		}, function() {
			$log.info('Modal dismissed at: ' + new Date());
		});
	};

	$scope.removeIndicadorAndamento = function(idIndicadorAndamento) {
		faseServicos.removeIndicadorAndamento(idFase).then(function(data) {
			$scope.carregaIndicadoresAndamento();
		});
	};

});

app.controller('IndicadorAndamentoModalCtrl', function($scope, $http, $log, $uibModalInstance, indicadorServicos,
		faseServicos, idProjeto, indicadorAndamento) {

	$scope.carregaIndicadores = function() {
		indicadorServicos.buscaIndicadoresPorProjeto(idProjeto).then(function(data) {
			$scope.indicadoresAssociados = data.data;
		});
	}
	$scope.carregaIndicadores();

	$scope.carregaFases = function() {
		faseServicos.buscaFasesPorProjeto(idProjeto).then(function(data) {
			$scope.fases = data.data;
		});
	}
	$scope.carregaFases();

	if (indicadorAndamento !== null) {
		$scope.indicadorAndamento = angular.copy(indicadorAndamento);
		$scope.indicadorAndamento.indicadorAssociado = $scope.indicadorAndamento.idIndicadorAssociado
	}

	$scope.ok = function() {
		$uibModalInstance.close(this.indicadorAndamento);
	};

	$scope.cancel = function() {
		$uibModalInstance.dismiss('cancel');
	};
});
