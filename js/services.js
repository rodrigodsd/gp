// FACTORY

app.factory("projetoServicos", [ '$http', function($http) {

	var servicoBase = '../controller/ProjetoSrv.php?'

	var obj = {};

	obj.buscaProjetos = function() {
		return $http.get(servicoBase + 'projetos=true');
	};

	obj.getProjeto = function(projetoId) {
		return $http.get(servicoBase + 'projetos=true&id=' + projetoId);
	};

	obj.salvaProjeto = function(projeto) {
		return $http.post(servicoBase + 'salvaProjeto', projeto).then(function(results) {
			return results;
		});
	};

	obj.salvaJustificativa = function(justificativa) {
		return $http.post(servicoBase + 'salvaJustificativa', justificativa).then(function(results) {
			return results;
		});
	};

	obj.removeProjeto = function(id) {
		return $http.post(servicoBase + 'removeProjeto=true&id=' + id).then(function(status) {
			return status.data;
		});
	};

	obj.salvaAcompanhamento = function(diaEmailSemanal, acompanhamento) {
		$http.post(servicoBase + 'salvaAcompanhamento', {
			"diaEmailSemanal" : diaEmailSemanal,
			"acompanhamento" : acompanhamento
		});
	}

	return obj;
} ]);

app.factory("indicadorServicos", [
		'$http',
		function($http) {

			var servicoBase = '../controller/IndicadorSrv.php?'

			var obj = {};

			obj.buscaIndicadores = function() {
				return $http.get(servicoBase + 'indicadores=true');
			};

			obj.getIndicador = function(projetoId) {
				return $http.get(servicoBase + 'indicadores=true&id=' + projetoId);
			};

			obj.salvaIndicador = function(projeto) {
				return $http.post(servicoBase + 'salvaIndicador', projeto).then(function(results) {
					return results;
				});
			};

			obj.removeIndicador = function(id) {
				return $http.post(servicoBase + 'removeIndicador=true&id=' + id).then(function(status) {
					return status.data;
				});
			};

			// --- INDICADORES ASSOCIADO

			obj.buscaIndicadoresParaAssoc = function(projetoId) {
				return $http.get(servicoBase + 'indicadoresParaAssoc=true&id=' + projetoId);
			};

			obj.buscaIndicadoresPorProjeto = function(projetoId) {
				return $http.get(servicoBase + 'indicadoresPorProjeto=true&id=' + projetoId);
			};

			obj.salvaIndicadorAssoc = function(indicador, projetoId) {
				return $http.post(servicoBase + 'salvaIndicadorAssociado=true&id=' + projetoId, indicador).then(
						function(results) {
							return results;
						});
			};

			obj.removeIndicadorAssoc = function(id) {
				return $http.post(servicoBase + 'removeIndicadorAssoc=true&id=' + id).then(function(status) {
					return status.data;
				});
			};

			// -- INDICADORES DE ANDAMENTO

			obj.buscaIndicadoresAndamento = function(projetoId) {
				return $http.get(servicoBase + 'indicadoresAndamento=true&id=' + projetoId);
			};

			obj.salvaIndicadorAndamento = function(metrica, projetoId) {
				return $http.post(servicoBase + 'salvaIndicadorAndamento=true', metrica).then(function(results) {
					return results;
				});
			};
			
			obj.buscaRelatorioIndicadoresAndamento = function() {
				return $http.get(servicoBase + 'indicadoresAndamento=true');
			};

			return obj;
		} ]);

app.factory("faseServicos", [ '$http', function($http) {

	var servicoBase = '../controller/FaseSrv.php?'

	var obj = {};

	obj.buscaFases = function(projetoId) {
		return $http.get(servicoBase + 'fases=true&id=' + projetoId);
	};

	obj.buscaFase = function(projetoId) {
		return $http.get(servicoBase + 'fase=true&id=' + projetoId);
	};

	obj.salvaFase = function(fase, projetoId) {
		return $http.post(servicoBase + 'salvaFase=true&id=' + projetoId, fase).then(function(results) {
			return results;
		});
	};

	obj.removeFase = function(id) {
		return $http.post(servicoBase + 'removeFase=true&id=' + id).then(function(status) {
			return status.data;
		});
	};

	obj.buscaFasesPorProjeto = function(projetoId) {
		return $http.get(servicoBase + 'fasesPorProjeto=true&id=' + projetoId);
	};

	return obj;
} ]);

app.factory("usuarioServicos", [ '$http', function($http) {

	var servicoBase = '../controller/UsuarioSrv.php?'

	var obj = {};

	obj.buscaUsuarios = function() {
		return $http.get(servicoBase + 'usuario=true');
	};

	obj.buscaUsuario = function(idUsuario) {
		return $http.get(servicoBase + 'usuario=true&id=' + idUsuario);
	};

	obj.salvaUsuario = function(usuario) {
		return $http.post(servicoBase + 'salvaUsuario=true', usuario).then(function(results) {
			return results;
		});
	};

	obj.removeUsuario = function(idUsuario) {
		return $http.post(servicoBase + 'removeUsuario=true&id=' + idUsuario).then(function(status) {
			return status.data;
		});
	};

	return obj;
} ]);
