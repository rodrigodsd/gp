var app = angular.module('gpApp', [ 'ngRoute', 'ui.bootstrap', 'ui.grid' ]);

// CONTROLLERS

app.controller('HeaderCtrl', function($scope, $location) {

	$scope.isActive = function(viewLocation) {

		return viewLocation === $location.path();
	};
});

app.controller('projetoListaCtrl', function($scope, $http, $log, projetoServicos, $uibModal) {

	$scope.carregaLista = function() {
		projetoServicos.buscaProjetos().then(function(data) {
			$scope.projetos = data.data;
		});
	}

	$scope.carregaLista();

	$scope.update = function(projeto) {
		var modalInstance = $uibModal.open({
			animation : $scope.animationsEnabled,
			templateUrl : 'ProjetoModal.php',
			controller : 'projetoModalCtrl',
			resolve : {
				projeto : function() {
					return projeto;
				}
			}
		});

		modalInstance.result.then(function(projetoEdited) {

			projetoServicos.salvaJustificativa({
				idProjeto : projetoEdited.idProjeto,
				justificativa : projetoEdited.justificativa,
				status : projetoEdited.status
			});

			projetoServicos.salvaProjeto(projetoEdited).then(function() {
				$scope.carregaLista();
			});
		}, function() {
			$log.info('Modal dismissed at: ' + new Date());
		});
	};
	
	$scope.removeProjeto = function(idProjeto) {
		projetoServicos.removeProjeto(idProjeto).then(function(data) {
			$scope.carregaLista();
		});
	};

	$scope.toggleAnimation = function() {
		$scope.animationsEnabled = !$scope.animationsEnabled;
	};

	$scope.translateStatus = function(status) {

		switch (status) {
		case "1":
			return 'Em análise'
			break;
		case "2":
			return 'Análise Realizada'
			break;
		case "3":
			return 'Análise Aprovada'
			break;
		case "4":
			return 'Iniciado'
			break;
		case "5":
			return 'Planejado'
			break;
		case "6":
			return 'Em Andamento'
			break;
		case "7":
			return 'Encerrado'
			break;
		case "8":
			return 'Cancelado'
			break;

		default:
			break;
		}
	};
});

app.controller('projetoModalCtrl', function($scope, $uibModalInstance, projeto) {

	$scope.projeto = angular.copy(projeto);

	if (projeto != null) {
		$scope.projeto.dataPrevTermino = new Date(projeto.dataPrevTermino);
		$scope.projeto.dataInicio = new Date(projeto.dataInicio);
	}

	$scope.ok = function() {
		$uibModalInstance.close($scope.projeto);
	};

	$scope.cancel = function() {
		$uibModalInstance.dismiss('cancel');
	};

});

// ------------------------------ DETALHE DO PROJETO

app.controller('projetoDetalheCtrl', function($scope, $http, $log, projetoServicos, indicadorServicos, faseServicos,
		$uibModal, $routeParams) {

	$scope.acompanhamento = false;
	// LOAD DATA

	$scope.carregaIndicadoresDoProjeto = function() {
		indicadorServicos.buscaIndicadoresPorProjeto($routeParams.idProjeto).then(function(data) {
			$scope.indicadores = data.data;
		});
	}

	$scope.carregaIndicadoresDoProjeto();

	$scope.carregaFases = function() {
		faseServicos.buscaFases($routeParams.idProjeto).then(function(data) {
			$scope.fases = data.data;
		});
	}

	$scope.carregaFases();
	
	// ACOMPANHAMENTO
	
	$scope.salvaCompanhamento = function(){
		projetoServicos.salvaAcompanhamento($scope.diaEmailSemanal,$scope.acompanhamento);
	}

	// MODAL DA GRID DE INDICADORES

	$scope.updateIndicador = function(indicadorAssociado) {
		var modalInstance = $uibModal.open({
			animation : $scope.animationsEnabled,
			templateUrl : 'IndicadorAssociadoModal.php',
			controller : 'IndicadorAssociadoModalCtrl',
			resolve : {
				idProjeto : function() {
					return $routeParams.idProjeto;
				},
				indicadorAssociado : function() {
					return indicadorAssociado;
				}
			}
		});

		modalInstance.result.then(function(indicador) {
			indicadorServicos.salvaIndicadorAssoc(indicador, $routeParams.idProjeto).then(function() {
				$scope.carregaIndicadoresDoProjeto();
			});
		}, function() {
			$log.info('Modal dismissed at: ' + new Date());
		});
	};

	// MODAL DA GRID DE FASES

	$scope.updateFase = function(fase) {
		var modalInstance = $uibModal.open({
			animation : $scope.animationsEnabled,
			templateUrl : 'FaseModal.php',
			controller : 'FaseModalCtrl',
			resolve : {
				fase : function() {
					return fase;
				}
			}
		});

		modalInstance.result.then(function(fase) {
			faseServicos.salvaFase(fase, $routeParams.idProjeto).then(function() {
				$scope.carregaFases();
			});
		}, function() {
			$log.info('Modal dismissed at: ' + new Date());
		});
	};

	// DELETE FUNCTIONS

	$scope.removeFase = function(idFase) {
		faseServicos.removeFase(idFase).then(function(data) {
			$scope.carregaFases();
		});
	};

	$scope.removeIndicador = function(idIndicadorAssoc) {
		indicadorServicos.removeIndicadorAssoc(idIndicadorAssoc).then(function(data) {
			$scope.carregaIndicadoresDoProjeto();
		});
	};

});

app.controller('IndicadorAssociadoModalCtrl', function($scope, $uibModalInstance, indicadorServicos, idProjeto,
		indicadorAssociado) {

	if (indicadorAssociado !== undefined) {
		$scope.indicadorAssociado = angular.copy(indicadorAssociado);
	}

	$scope.carregaIndicadores = function() {
		indicadorServicos.buscaIndicadoresParaAssoc(idProjeto).then(function(data) {
			$scope.indicadores = data.data;
		});
	}
	$scope.carregaIndicadores();

	$scope.print = function(indicadorSelected) {
		$scope.indicadorAssociado = angular.copy(indicadorSelected);
	}

	$scope.ok = function() {
		$uibModalInstance.close(this.indicadorAssociado);
	};

	$scope.cancel = function() {
		$uibModalInstance.dismiss('cancel');
	};

});

app.controller('FaseModalCtrl', function($scope, $uibModalInstance, faseServicos, fase) {
	if (fase !== undefined) {
		$scope.fase = angular.copy(fase);
	}

	$scope.ok = function() {
		$uibModalInstance.close(this.fase);
	};

	$scope.cancel = function() {
		$uibModalInstance.dismiss('cancel');
	};

});
