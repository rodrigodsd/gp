// CONTROLLERS
app.controller('HeaderCtrl', function($scope, $location) {

	$scope.isActive = function(viewLocation) {

		return viewLocation === $location.path();
	};
});

app.controller('usuarioCtrl', function($scope, $location, $log, usuarioServicos, $uibModal) {
	$scope.carregaLista = function() {
		usuarioServicos.buscaUsuarios().then(function(data) {
			$scope.usuarios = data.data;
		});
	}

	$scope.carregaLista();

	$scope.update = function(usuario) {
		var modalInstance = $uibModal.open({
			animation : $scope.animationsEnabled,
			templateUrl : 'UsuarioModal.php',
			controller : 'usuarioModalCtrl',
			resolve : {
				usuario : function() {
					return usuario;
				}
			}
		});

		modalInstance.result.then(function(usuarioEdited) {

			usuarioServicos.salvaUsuario(usuarioEdited).then(function() {
				$scope.carregaLista();
			});
		}, function() {
			$log.info('Modal dismissed at: ' + new Date());
		});
	};

	$scope.toggleAnimation = function() {
		$scope.animationsEnabled = !$scope.animationsEnabled;
	};

	$scope.removeUsuario = function(idUsuario) {
		usuarioServicos.removeUsuario(idUsuario).then(function(data) {
			$scope.carregaLista();
		});
	};
});

app.controller('usuarioModalCtrl', function($scope, $location, $uibModalInstance, usuario) {

	$scope.usuario = angular.copy(usuario);

	$scope.ok = function() {
		$uibModalInstance.close($scope.usuario);
	};

	$scope.cancel = function() {
		$uibModalInstance.dismiss('cancel');
	};
});
