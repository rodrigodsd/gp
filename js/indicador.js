// CONTROLLERS

app.controller('HeaderCtrl', function($scope, $location) {

	$scope.isActive = function(viewLocation) {

		return viewLocation === $location.path();
	};
});

app.controller('indicadorListaCtrl', function($scope, $http, $log, indicadorServicos, $uibModal) {

	$scope.carregaLista = function() {
		indicadorServicos.buscaIndicadores().then(function(data) {
			$scope.indicadores = data.data;
		});
	}

	$scope.carregaLista();

	$scope.update = function(indicador) {
		var modalInstance = $uibModal.open({
			animation : $scope.animationsEnabled,
			templateUrl : 'IndicadorModal.php',
			controller : 'indicadorModalCtrl',
			resolve : {
				indicador : function() {
					return indicador;
				}
			}
		});

		modalInstance.result.then(function(indicadorEdited) {
					
			indicadorServicos.salvaIndicador(indicadorEdited).then(function() {
				$scope.carregaLista();
			});
		}, function() {
			$log.info('Modal dismissed at: ' + new Date());
		});
	};

	$scope.toggleAnimation = function() {
		$scope.animationsEnabled = !$scope.animationsEnabled;
	};
	
	$scope.removeIndicador = function(idIndicador) {
		indicadorServicos.removeIndicador(idIndicador).then(function(data) {
			$scope.carregaLista();
		});
	};

});

app.controller('indicadorModalCtrl', function($scope, $uibModalInstance, indicador) {

	$scope.indicador = angular.copy(indicador);
	
	if(indicador != null){
		$scope.indicador.dataPrevTermino = new Date(indicador.dataPrevTermino);
		$scope.indicador.dataInicio = new Date(indicador.dataInicio);
	}
	
	$scope.ok = function() {
		$uibModalInstance.close($scope.indicador);
	};

	$scope.cancel = function() {
		$uibModalInstance.dismiss('cancel');
	};

});


