<?php

class Fase {

	public $idFase;

	public $nome;

	public $obs;

	public function getIdFase() {

		return $this->idFase;
	}

	public function setIdFase($idFase) {

		$this->idFase = $idFase;
		return $this;
	}

	public function getNome() {

		return $this->nome;
	}

	public function setNome($nome) {

		$this->nome = $nome;
		return $this;
	}

	public function getObs() {

		return $this->obs;
	}

	public function setObs($obs) {

		$this->obs = $obs;
		return $this;
	}

}

?>