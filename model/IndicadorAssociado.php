<?php

class IndicadorAssociado {

	public $idIndicadorAssociado;

	public $idProjeto;

	public $idIndicador;

	public $min;

	public $max;

	public function getIdIndicadorAssociado() {

		return $this->idIndicadorAssociado;
	}

	public function setIdIndicadorAssociado($idIndicadorAssociado) {

		$this->idIndicadorAssociado = $idIndicadorAssociado;
		return $this;
	}

	public function getIdProjeto() {

		return $this->idProjeto;
	}

	public function setIdProjeto($idProjeto) {

		$this->idProjeto = $idProjeto;
		return $this;
	}

	public function getIdIndicador() {

		return $this->idIndicador;
	}

	public function setIdIndicador($idIndicador) {

		$this->idIndicador = $idIndicador;
		return $this;
	}

	public function getMin() {

		return $this->min;
	}

	public function setMin($min) {

		$this->min = $min;
		return $this;
	}

	public function getMax() {

		return $this->max;
	}

	public function setMax($max) {

		$this->max = $max;
		return $this;
	}

}

?>