<?php

class Indicador {

	public $idIndicador;

	public $nome;

	public $descricao;

	public function getIdIndicador() {

		return $this->idIndicador;
	}

	public function setIdIndicador($idIndicador) {

		$this->idIndicador = $idIndicador;
		return $this;
	}

	public function getNome() {

		return $this->nome;
	}

	public function setNome($nome) {

		$this->nome = $nome;
		return $this;
	}

	public function getDescricao() {

		return $this->descricao;
	}

	public function setDescricao($descricao) {

		$this->descricao = $descricao;
		return $this;
	}

}

?>