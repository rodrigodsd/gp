<?php

class Usuario {

	private $idUsuario;

	private $nome;

	private $perfil;

	private $login;

	private $senha;

	public function getIdUsuario() {

		return $this->idUsuario;
	}

	public function setIdUsuario($idUsuario) {

		$this->idUsuario = $idUsuario;
		return $this;
	}

	public function getNome() {

		return $this->nome;
	}

	public function setNome($nome) {

		$this->nome = $nome;
		return $this;
	}

	public function getPerfil() {

		return $this->perfil;
	}

	public function setPerfil($perfil) {

		$this->perfil = $perfil;
		return $this;
	}

	public function getLogin() {

		return $this->login;
	}

	public function setLogin($login) {

		$this->login = $login;
		return $this;
	}

	public function getSenha() {

		return $this->senha;
	}

	public function setSenha($senha) {

		$this->senha = $senha;
		return $this;
	}

}

?>