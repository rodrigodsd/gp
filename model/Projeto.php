<?php

class Projeto {

	public $idProjeto;

	public $nome;

	public $gerente;

	public $orcamentoTotal;

	public $descricao;

	public $status;

	public $risco;

	public $dataInicio;

	public $dataPrevTermino;

	public $dataTermino;
	
	public $colaboradores;

	public function __construct() {
		
	}

	public function getIdProjeto() {

		return $this->idProjeto;
	}

	public function setIdProjeto($idProjeto) {

		$this->idProjeto = $idProjeto;
		return $this;
	}

	public function getNome() {

		return $this->nome;
	}

	public function setNome($nome) {

		$this->nome = $nome;
		return $this;
	}

	public function getGerente() {

		return $this->gerente;
	}

	public function setGerente($gerente) {

		$this->gerente = $gerente;
		return $this;
	}

	public function getOrcamentoTotal() {

		return $this->orcamentoTotal;
	}

	public function setOrcamentoTotal($orcamentoTotal) {

		$this->orcamentoTotal = $orcamentoTotal;
		return $this;
	}

	public function getDescricao() {

		return $this->descricao;
	}

	public function setDescricao($descricao) {

		$this->descricao = $descricao;
		return $this;
	}

	public function getStatus() {

		return $this->status;
	}

	public function setStatus($status) {

		$this->status = $status;
		return $this;
	}

	public function getRisco() {

		return $this->risco;
	}

	public function setRisco($risco) {

		$this->risco = $risco;
		return $this;
	}

	public function getDataInicio() {

		return $this->dataInicio;
	}

	public function setDataInicio($dataInicio) {

		$this->dataInicio = $dataInicio;
		return $this;
	}

	public function getDataPrevTermino() {

		return $this->dataPrevTermino;
	}

	public function setDataPrevTermino($dataPrevTermino) {

		$this->dataPrevTermino = $dataPrevTermino;
		return $this;
	}

	public function getDataTermino() {

		return $this->dataTermino;
	}

	public function setDataTermino($dataTermino) {

		$this->dataTermino = $dataTermino;
		return $this;
	}

	public function getColaboradores() {

		return $this->colaboradores;
	}

	public function setColaboradores($colaboradores) {

		$this->colaboradores = $colaboradores;
		return $this;
	}
	

}

?>