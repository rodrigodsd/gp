<?php
include_once 'Sessao.php';
include_once '../dao/FaseDao.php';

class FaseSrv {

	private $faseDao;

	function __construct() {

		$this->faseDao = new FaseDao ();
	}

	function buscaFase($id) {
		
		try {
			Logger ( "Buscando fase : " . $id );
			echo json_encode ( $this->faseDao->find ( $id ) );
		} catch ( Exception $e ) {
			Logger ( "(file:" . $e->getFile () . ",line:" . $e->getLine () . ") message : " . $e->getMessage () );
		}
	}

	function buscaFases( $id ) {
		
		try {
			echo json_encode ( $this->faseDao->findAll ( $id ) );
		} catch ( Exception $e ) {
			Logger ( "(file:" . $e->getFile () . ",line:" . $e->getLine () . ") message : " . $e->getMessage () );
		}
	}
	
	function salvaFase($fase, $idProjeto) {
	
		try {
			echo json_encode ( $this->faseDao->save($fase, $idProjeto) );
		} catch ( Exception $e ) {
			Logger ( "(file:" . $e->getFile () . ",line:" . $e->getLine () . ") message : " . $e->getMessage () );
		}
	}
	
	function remove($idFase) {
	
		try {
			echo json_encode ( $this->faseDao->remove($idFase) );
		} catch ( Exception $e ) {
			Logger ( "(file:" . $e->getFile () . ",line:" . $e->getLine () . ") message : " . $e->getMessage () );
		}
	}
	
	function fasesPorProjeto($idProjeto){
		try {
			echo json_encode ( $this->faseDao->fasesPorProjeto ($idProjeto) );
		} catch ( Exception $e ) {
			Logger ( "(file:" . $e->getFile () . ",line:" . $e->getLine () . ") message : " . $e->getMessage () );
		}
	
	}
		
}

$faseSrv = new FaseSrv ();

if (isset ( $_GET ["fases"] )) {
	if (isset ( $_GET ["id"] )) {
		$faseSrv->buscaFases ( $_GET ["id"] );
	} 
}

if (isset ( $_GET ["fase"] )) {
	if (isset ( $_GET ["id"] )) {
		$faseSrv->buscaFase ( $_GET ["id"] );
	} 
}

if (isset ( $_GET ["salvaFase"] )) {
	$faseSrv->salvaFase(json_decode(file_get_contents("php://input")),$_GET ["id"] );
}

if (isset ( $_GET ["removeFase"] )) {
	$faseSrv->remove($_GET ["id"]);
}

if (isset ( $_GET ["fasesPorProjeto"] )) {
	if (isset ( $_GET ["id"] )) {
		$faseSrv->fasesPorProjeto ( $_GET ["id"] );
	}
}


?>