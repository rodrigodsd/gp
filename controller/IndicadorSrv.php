<?php
include_once 'Sessao.php';
include_once '../dao/IndicadorDao.php';

class IndicadorSrv {

	private $indicadorDao;

	function __construct() {

		$this->indicadorDao = new IndicadorDao ();
	}

	function buscaIndicador($id) {

		try {
			Logger ( "Buscando indicador : " . $id );
			echo json_encode ( $this->indicadorDao->find ( $id ) );
		} catch ( Exception $e ) {
			Logger ( "(file:" . $e->getFile () . ",line:" . $e->getLine () . ") message : " . $e->getMessage () );
		}
	}

	function buscaIndicadores() {

		try {
			echo json_encode ( $this->indicadorDao->findAll () );
		} catch ( Exception $e ) {
			Logger ( "(file:" . $e->getFile () . ",line:" . $e->getLine () . ") message : " . $e->getMessage () );
		}
	}

	function salvaIndicador($indicador) {

		try {
			echo json_encode ( $this->indicadorDao->save ( $indicador ) );
		} catch ( Exception $e ) {
			Logger ( "(file:" . $e->getFile () . ",line:" . $e->getLine () . ") message : " . $e->getMessage () );
		}
	}

	function indicadoresParaAssoc($idProjeto) {

		try {
			echo json_encode ( $this->indicadorDao->indicadoresParaAssoc ( $idProjeto ) );
		} catch ( Exception $e ) {
			Logger ( "(file:" . $e->getFile () . ",line:" . $e->getLine () . ") message : " . $e->getMessage () );
		}
	}

	function indicadoresPorProjeto($idProjeto) {

		try {
			echo json_encode ( $this->indicadorDao->indicadoresPorProjeto ( $idProjeto ) );
		} catch ( Exception $e ) {
			Logger ( "(file:" . $e->getFile () . ",line:" . $e->getLine () . ") message : " . $e->getMessage () );
		}
	}

	/*
	 * function salvaIndicadoresAssoc($indicadoresAssociado, $idProjeto) {
	 *
	 * try {
	 * echo json_encode ( $this->indicadorDao->salvaIndicadoresAssoc($indicadoresAssociado, $idProjeto) );
	 * } catch ( Exception $e ) {
	 * Logger ( "(file:" . $e->getFile () . ",line:" . $e->getLine () . ") message : " . $e->getMessage () );
	 * }
	 * }
	 */
	function salvaIndicadorAssociado($indicadorAssociado, $idProjeto) {

		try {
			echo json_encode ( $this->indicadorDao->salvaIndicadorAssociado ( $indicadorAssociado, $idProjeto ) );
		} catch ( Exception $e ) {
			Logger ( "(file:" . $e->getFile () . ",line:" . $e->getLine () . ") message : " . $e->getMessage () );
		}
	}

	function removeIndicadorAssoc($idIndicadorAssoc) {

		try {
			echo json_encode ( $this->indicadorDao->removeIndicadorAssoc ( $idIndicadorAssoc ) );
		} catch ( Exception $e ) {
			Logger ( "(file:" . $e->getFile () . ",line:" . $e->getLine () . ") message : " . $e->getMessage () );
		}
	}

	function removeIndicador($idIndicador) {

		try {
			echo json_encode ( $this->indicadorDao->removeIndicador ( $idIndicador ) );
		} catch ( Exception $e ) {
			Logger ( "(file:" . $e->getFile () . ",line:" . $e->getLine () . ") message : " . $e->getMessage () );
		}
	}

	function indicadoresAndamento($idProjeto) {

		try {
			echo json_encode ( $this->indicadorDao->indicadoresAndamento ( $idProjeto ) );
		} catch ( Exception $e ) {
			Logger ( "(file:" . $e->getFile () . ",line:" . $e->getLine () . ") message : " . $e->getMessage () );
		}
	}
	
	function relatorioIndicadoresAndamento() {
	
		try {
			echo json_encode ( $this->indicadorDao->relatorioIndicadoresAndamento ( ) );
		} catch ( Exception $e ) {
			Logger ( "(file:" . $e->getFile () . ",line:" . $e->getLine () . ") message : " . $e->getMessage () );
		}
	}

	function salvaIndicadorAndamento($indicadorAndamento) {

		try {
			echo json_encode ( $this->indicadorDao->salvaIndicadorAndamento ( $indicadorAndamento ) );
		} catch ( Exception $e ) {
			Logger ( "(file:" . $e->getFile () . ",line:" . $e->getLine () . ") message : " . $e->getMessage () );
		}
	}

}

$indicadorSrv = new IndicadorSrv ();

if (isset ( $_GET ["indicadores"] )) {
	if (isset ( $_GET ["id"] )) {
		$indicadorSrv->buscaIndicador ( $_GET ["id"] );
	} else {
		$indicadorSrv->buscaIndicadores ();
	}
}

if (isset ( $_GET ["salvaIndicador"] )) {
	$indicadorSrv->salvaIndicador ( json_decode ( file_get_contents ( "php://input" ) ) );
}

if (isset ( $_GET ["removeIndicador"] )) {
	$indicadorSrv->removeIndicador ( $_GET ["id"] );
}

if (isset ( $_GET ["removeIndicadorAssoc"] )) {
	$indicadorSrv->removeIndicadorAssoc ( $_GET ["id"] );
}

if (isset ( $_GET ["indicadoresParaAssoc"] )) {
	if (isset ( $_GET ["id"] )) {
		$indicadorSrv->indicadoresParaAssoc ( $_GET ["id"] );
	}
}

if (isset ( $_GET ["indicadoresPorProjeto"] )) {
	if (isset ( $_GET ["id"] )) {
		$indicadorSrv->indicadoresPorProjeto ( $_GET ["id"] );
	}
}

/*
 * if (isset ( $_GET ["salvaIndicadoresAssoc"] )) {
 * $indicadorSrv->salvaIndicadoresAssoc(json_decode(file_get_contents("php://input")) , $_GET["id"]);
 * }
 */

if (isset ( $_GET ["salvaIndicadorAssociado"] )) {
	$indicadorSrv->salvaIndicadorAssociado ( json_decode ( file_get_contents ( "php://input" ) ), $_GET ["id"] );
}

if (isset ( $_GET ["salvaIndicadorAndamento"] )) {
	$indicadorSrv->salvaIndicadorAndamento ( json_decode ( file_get_contents ( "php://input" ) ) );
}

if (isset ( $_GET ["indicadoresAndamento"] )) {
	if (isset ( $_GET ["id"] )) {
		$indicadorSrv->indicadoresAndamento ( $_GET ["id"] );
	}else{
		$indicadorSrv->relatorioIndicadoresAndamento ( );
	}
}

?>