<?php
include_once 'Sessao.php';
include_once '../model/Usuario.php';
include_once '../dao/UsuarioDao.php';

class UsuarioSrv {

	private $usuarioDao;

	function __construct() {

		$this->usuarioDao = new UsuarioDao ();
	}

	function buscaUsuario($id) {

		try {
			Logger ( "Buscando usuario : " . $id );
			echo json_encode ( $this->usuarioDao->find ( $id ) );
		} catch ( Exception $e ) {
			Logger ( "(file:" . $e->getFile () . ",line:" . $e->getLine () . ") message : " . $e->getMessage () );
		}
	}

	function buscaUsuarios() {

		try {
			echo json_encode ( $this->usuarioDao->findAll () );
		} catch ( Exception $e ) {
			Logger ( "(file:" . $e->getFile () . ",line:" . $e->getLine () . ") message : " . $e->getMessage () );
		}
	}

	function salvaUsuario($usuario) {

		try {
			echo json_encode ( $this->usuarioDao->update ( $usuario ) );
		} catch ( Exception $e ) {
			Logger ( "(file:" . $e->getFile () . ",line:" . $e->getLine () . ") message : " . $e->getMessage () );
		}
	}

	function removeUsuario($idUsuario) {

		try {
			echo json_encode ( $this->usuarioDao->delete ( $idUsuario ) );
		} catch ( Exception $e ) {
			Logger ( "(file:" . $e->getFile () . ",line:" . $e->getLine () . ") message : " . $e->getMessage () );
		}
	}

	function cadastraUsuario(Usuario $usuario) {

		try {
			$this->usuarioDao->insere ( $usuario );
			header ( "location:../index.php" );
		} catch ( Exception $e ) {
			Logger ( "(file:" . $e->getFile () . ",line:" . $e->getLine () . ") message : " . $e->getMessage () );
		}
	}

}

$usuarioSrv = new UsuarioSrv ();

if (isset ( $_GET ["usuario"] )) {
	if (isset ( $_GET ["id"] )) {
		$usuarioSrv->buscaUsuario ( $_GET ["id"] );
	} else {
		$usuarioSrv->buscaUsuarios ();
	}
}

if (isset ( $_GET ["salvaUsuario"] )) {
	$usuarioSrv->salvaUsuario ( json_decode ( file_get_contents ( "php://input" ) ) );
}
if (isset ( $_POST ["cadastraUsuario"] )) {
	
	$usuario = new Usuario ();
	
	$usuario->setNome ( $_POST ["usernamesignup"] );
	$usuario->setLogin ( $_POST ["loginsignup"] );
	$usuario->setSenha ( md5 ( $_POST ["passwordsignup"] ) );
	$usuario->setPerfil ( "USUARIO" );
	
	$usuarioSrv->cadastraUsuario ( $usuario );
}

if (isset ( $_GET ["removeUsuario"] )) {
	$usuarioSrv->removeUsuario ( $_GET ["id"] );
}

?>