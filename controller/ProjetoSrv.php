<?php
include_once 'Sessao.php';
include_once '../dao/ProjetosDao.php';

class ProjetoSrv {

	private $projetoDao;

	function __construct() {

		$this->projetoDao = new ProjetoDao ();
	}

	function buscaProjeto($id) {
		
		try {
			echo json_encode ( $this->projetoDao->find ( $id ) );
		} catch ( Exception $e ) {
			Logger ( "(file:" . $e->getFile () . ",line:" . $e->getLine () . ") message : " . $e->getMessage () );
		}
	}

	function buscaProjetos() {
		
		try {
			echo json_encode ( $this->projetoDao->findAll () );
		} catch ( Exception $e ) {
			Logger ( "(file:" . $e->getFile () . ",line:" . $e->getLine () . ") message : " . $e->getMessage () );
		}
	}
	
	function salvaProjeto($projeto) {
	
		try {
			echo json_encode ( $this->projetoDao->save($projeto) );
		} catch ( Exception $e ) {
			Logger ( "(file:" . $e->getFile () . ",line:" . $e->getLine () . ") message : " . $e->getMessage () );
		}
	}
	
	function salvaJustificativa($justificativa) {
	
		try {
			$this->projetoDao->salvaJustificativa($justificativa);
		} catch ( Exception $e ) {
			Logger ( "(file:" . $e->getFile () . ",line:" . $e->getLine () . ") message : " . $e->getMessage () );
		}
	}
	
	function removeProjeto($idProjeto){
		try {
			$this->projetoDao->removeProjeto($idProjeto);
		} catch ( Exception $e ) {
			Logger ( "(file:" . $e->getFile () . ",line:" . $e->getLine () . ") message : " . $e->getMessage () );
		}
	}
}

$projetoSrv = new ProjetoSrv ();

if (isset ( $_GET ["projetos"] )) {
	if (isset ( $_GET ["id"] )) {
		$projetoSrv->buscaProjeto ( $_GET ["id"] );
	} else {
		$projetoSrv->buscaProjetos ();
	}
}

if (isset ( $_GET ["salvaProjeto"] )) {
	$projetoSrv->salvaProjeto(json_decode(file_get_contents("php://input")));
}

if (isset ( $_GET ["salvaJustificativa"] )) {
	$projetoSrv->salvaJustificativa(json_decode(file_get_contents("php://input")));
}

if (isset ( $_GET ["removeProjeto"] )) {
	$projetoSrv->removeProjeto( $_GET ["id"] );
}

?>